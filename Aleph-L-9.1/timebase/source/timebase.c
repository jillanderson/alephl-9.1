#include "../timebase.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stddef.h>

static struct timebase {
	timebase_handler_ft *handler;
	void *arg;
} timebase = { 0 };

void timebase_start(void)
{
	/* konfiguracja częstotliwości 1kHz */
	static uint8_t const PRESCALER_32 = 3U;
	static uint8_t const TOP = 249U;

	TCCR2 = (uint8_t) ((1U << WGM21) + PRESCALER_32);
	TIFR |= (1U << OCF2); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK |= (1U << OCIE2);
	OCR2 = TOP;
	TCNT2 = 0U;
}

void timebase_handler(timebase_handler_ft *handler, void *arg)
{
	timebase.handler = handler;
	timebase.arg = arg;
}

ISR(TIMER2_COMP_vect)
{
	if (NULL != timebase.handler) {
		(timebase.handler)(timebase.arg);
	}
}

