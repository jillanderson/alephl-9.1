#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikami linii wejściowych
 *
 ******************************************************************************/

#include <stdbool.h>

typedef enum {
	INPUT_SIGNAL_0,
	INPUT_SIGNAL_1,
	INPUT_SIGNAL_2,
	INPUT_SIGNAL_3,
	INPUT_SIGNAL_QTY,
	INPUT_SIGNAL_ALL = INPUT_SIGNAL_QTY,
	INPUT_SIGNAL_NONE
} input_signal_et;

/*
 * inicjuje sterowanie wejściami sygnału
 * wyłącza wszystkie wejścia
 */
void hw_input_signal_init(void);

/*
 * włącza zadaną linię wejściową
 *
 * @input	linia wejściowa
 */
void hw_input_signal_on(input_signal_et input);

/*
 * wyłącza zadaną linię wejściową
 *
 * @input	linia wejściowa
 */
void hw_input_signal_off(input_signal_et input);

/*
 * przełącza zadaną linię wejściową w stan przeciwny
 *
 * @input	linia wejściowa
 */
void hw_input_signal_toggle(input_signal_et input);

/*
 * sprawdza czy linia wejściowa jest włączona
 *
 * @ret		true - linia włączona
 *		false - linia wyłączona
 */
bool hw_input_signal_is_on(input_signal_et input);

