#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikami tłumika sygnału
 *
 ******************************************************************************/

#include <stdbool.h>

typedef enum {
	ATTENUATOR_1DB,
	ATTENUATOR_2DB,
	ATTENUATOR_4DB,
	ATTENUATOR_8DB,
	ATTENUATOR_16DB,
	ATTENUATOR_32DB,
	ATTENUATOR_QTY
} attenuator_et;

/*
 * inicjuje sterowanie filtrami tłumika
 * ustawia maksymalne tłumienie
 */
void hw_attenuator_init(void);

/*
 * włącza filtr tłumika
 *
 * @attenuator	filtr tłumika
 */
void hw_attenuator_on(attenuator_et attenuator);

/*
 * wyłącza filtr tłumika
 *
 * @attenuator	filtr tłumika
 */
void hw_attenuator_off(attenuator_et attenuator);

/*
 * przłącza filtr tłumika w stan przeciwny
 *
 * @attenuator	filtr tłumika
 */
void hw_attenuator_toggle(attenuator_et attenuator);

/*
 * sprawdza czy filtr tłumika jest włączony
 *
 * @ret		true - filtr włączony
 *		false - filtr wyłączony
 */
bool hw_attenuator_is_on(attenuator_et attenuator);

