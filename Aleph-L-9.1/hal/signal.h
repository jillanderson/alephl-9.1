#pragma once

/** \file **********************************************************************
 *
 * sterowanie wskaźnikami optycznymi linii wejściowych
 *
 ******************************************************************************/

#include <stdbool.h>

typedef enum {
	SIGNAL_INDICATOR_0,
	SIGNAL_INDICATOR_1,
	SIGNAL_INDICATOR_2,
	SIGNAL_INDICATOR_3,
	SIGNAL_INDICATOR_QTY,
	SIGNAL_INDICATOR_ALL = SIGNAL_INDICATOR_QTY
} signal_indicator_et;

/*
 * inicjuje sterowanie wskaźnikami linii wejściowych
 * wyłącza wszystkie wskaźniki
 */
void hw_signal_indicator_init(void);

/*
 * włącza zadany wskaźnik linii wejściowej
 *
 * @signal	wskaźnik
 */
void hw_signal_indicator_on(signal_indicator_et signal);

/*
 * wyłącza zadany wskaźnik linii wejściowej
 *
 * @signal	wskaźnik
 */
void hw_signal_indicator_off(signal_indicator_et signal);

/*
 * przełącza zadany wskaźnik linii wejściowej w stan przeciwny
 *
 * @signal	wskaźnik
 */
void hw_signal_indicator_toggle(signal_indicator_et signal);

/*
 * sprawdza czy wskaźnik linii wejściowej jest włączony
 *
 * @ret		true - wskaźnik włączony
 *		false - wskaźnik wyłączony
 */
bool hw_signal_indicator_is_on(signal_indicator_et signal);

