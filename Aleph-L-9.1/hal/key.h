#pragma once

/** \file **********************************************************************
 *
 * sprawdzenie stanu przycisku
 *
 * przycisk w stanie aktywnym (wciśnięty) zwiera pin do masy
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje obsługę przycisku
 */
void hw_key_init(void);

/*
 * sprawdza czy przycisk jest wciśnięty
 *
 * @ret		true - przycisk wciśnięty
 *		false - przycisk zwolniony
 */
bool hw_key_is_pressed(void);

