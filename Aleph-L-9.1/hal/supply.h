#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikiem kontrolującym zasilanie
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje sterowanie zasilaniem
 */
void hw_supply_init(void);

/*
 * włącza zasilanie
 */
void hw_supply_on(void);

/*
 * wyłącza zasilanie
 */
void hw_supply_off(void);

/*
 * sprawdza czy zasilanie jest włączone
 *
 * @ret		true - wskaźnik włączony
 *		false - wskaźnik wyłączony
 */
bool hw_supply_is_on(void);

