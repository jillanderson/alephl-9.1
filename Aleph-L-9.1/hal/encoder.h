#pragma once

/** \file **********************************************************************
 *
 * odczytuje stan kanałów data i clk enkodera
 *
 ******************************************************************************/

#include <stdint.h>

typedef enum {
	ENCODER_INPUT,
	ENCODER_VOLUME,
	ENCODER_QTY
} encoder_et;

/*
 * inicjuje obsługę encoderów
 */
void hw_encoder_init(void);

/*
 * odczytuje kanały data i clk encodera
 *
 * @ret		stan kanałów w kodzie graya
 */
uint8_t hw_encoder_read_state(encoder_et encoder);

