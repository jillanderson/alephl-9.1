#include "../encoder.h"

#include <avr/io.h>
#include <common/gray_code.h>
#include <mega/io.h>
#include <stdint.h>

static io_pin_st const ENCODER_0_CHANNEL_A = { .port = &PORTD, .pin = PD0 };
static io_pin_st const ENCODER_0_CHANNEL_B = { .port = &PORTD, .pin = PD1 };
static io_pin_st const ENCODER_1_CHANNEL_A = { .port = &PORTD, .pin = PD4 };
static io_pin_st const ENCODER_1_CHANNEL_B = { .port = &PORTD, .pin = PD5 };

void hw_encoder_init(void)
{
	io_configure_as_input(&ENCODER_0_CHANNEL_A);
	io_configure_as_input(&ENCODER_0_CHANNEL_B);
	io_configure_as_input(&ENCODER_1_CHANNEL_A);
	io_configure_as_input(&ENCODER_1_CHANNEL_B);

	io_set_low(&ENCODER_0_CHANNEL_A);
	io_set_low(&ENCODER_0_CHANNEL_B);
	io_set_low(&ENCODER_1_CHANNEL_A);
	io_set_low(&ENCODER_1_CHANNEL_B);
}

uint8_t hw_encoder_read_state(encoder_et encoder)
{
	uint8_t data = 0U;
	uint8_t clk = 0U;

	switch (encoder) {
	case ENCODER_INPUT:
		data = io_read_pin(&ENCODER_0_CHANNEL_A);
		clk = io_read_pin(&ENCODER_0_CHANNEL_B);
		break;

	case ENCODER_VOLUME:
		data = io_read_pin(&ENCODER_1_CHANNEL_A);
		clk = io_read_pin(&ENCODER_1_CHANNEL_B);
		break;

	default:
		;
		break;
	}

	uint8_t code = gray_code(data, clk);

	return (code);
}

