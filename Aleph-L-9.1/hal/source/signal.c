#include "../signal.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const LED_SIGNAL_INDICATOR_0 = { .port = &PORTC, .pin = PC4 };
static io_pin_st const LED_SIGNAL_INDICATOR_1 = { .port = &PORTC, .pin = PC5 };
static io_pin_st const LED_SIGNAL_INDICATOR_2 = { .port = &PORTC, .pin = PC6 };
static io_pin_st const LED_SIGNAL_INDICATOR_3 = { .port = &PORTC, .pin = PC7 };

void hw_signal_indicator_init(void)
{
	io_configure_as_output(&LED_SIGNAL_INDICATOR_0);
	io_configure_as_output(&LED_SIGNAL_INDICATOR_1);
	io_configure_as_output(&LED_SIGNAL_INDICATOR_2);
	io_configure_as_output(&LED_SIGNAL_INDICATOR_3);

	io_set_low(&LED_SIGNAL_INDICATOR_0);
	io_set_low(&LED_SIGNAL_INDICATOR_1);
	io_set_low(&LED_SIGNAL_INDICATOR_2);
	io_set_low(&LED_SIGNAL_INDICATOR_3);
}

void hw_signal_indicator_on(signal_indicator_et signal)
{
	switch (signal) {
	case SIGNAL_INDICATOR_0:
		io_set_high(&LED_SIGNAL_INDICATOR_0);
		break;

	case SIGNAL_INDICATOR_1:
		io_set_high(&LED_SIGNAL_INDICATOR_1);
		break;

	case SIGNAL_INDICATOR_2:
		io_set_high(&LED_SIGNAL_INDICATOR_2);
		break;

	case SIGNAL_INDICATOR_3:
		io_set_high(&LED_SIGNAL_INDICATOR_3);
		break;

	case SIGNAL_INDICATOR_ALL:
		io_set_high(&LED_SIGNAL_INDICATOR_0);
		io_set_high(&LED_SIGNAL_INDICATOR_1);
		io_set_high(&LED_SIGNAL_INDICATOR_2);
		io_set_high(&LED_SIGNAL_INDICATOR_3);
		break;

	default:
		;
		break;
	}
}

void hw_signal_indicator_off(signal_indicator_et signal)
{
	switch (signal) {
	case SIGNAL_INDICATOR_0:
		io_set_low(&LED_SIGNAL_INDICATOR_0);
		break;

	case SIGNAL_INDICATOR_1:
		io_set_low(&LED_SIGNAL_INDICATOR_1);
		break;

	case SIGNAL_INDICATOR_2:
		io_set_low(&LED_SIGNAL_INDICATOR_2);
		break;

	case SIGNAL_INDICATOR_3:
		io_set_low(&LED_SIGNAL_INDICATOR_3);
		break;

	case SIGNAL_INDICATOR_ALL:
		io_set_low(&LED_SIGNAL_INDICATOR_0);
		io_set_low(&LED_SIGNAL_INDICATOR_1);
		io_set_low(&LED_SIGNAL_INDICATOR_2);
		io_set_low(&LED_SIGNAL_INDICATOR_3);
		break;

	default:
		;
		break;
	}
}

void hw_signal_indicator_toggle(signal_indicator_et signal)
{
	switch (signal) {
	case SIGNAL_INDICATOR_0:
		io_set_opposite(&LED_SIGNAL_INDICATOR_0);
		break;

	case SIGNAL_INDICATOR_1:
		io_set_opposite(&LED_SIGNAL_INDICATOR_1);
		break;

	case SIGNAL_INDICATOR_2:
		io_set_opposite(&LED_SIGNAL_INDICATOR_2);
		break;

	case SIGNAL_INDICATOR_3:
		io_set_opposite(&LED_SIGNAL_INDICATOR_3);
		break;

	case SIGNAL_INDICATOR_ALL:
		io_set_opposite(&LED_SIGNAL_INDICATOR_0);
		io_set_opposite(&LED_SIGNAL_INDICATOR_1);
		io_set_opposite(&LED_SIGNAL_INDICATOR_2);
		io_set_opposite(&LED_SIGNAL_INDICATOR_3);
		break;

	default:
		;
		break;
	}
}

bool hw_signal_indicator_is_on(signal_indicator_et signal)
{
	bool tmp = false;

	switch (signal) {
	case SIGNAL_INDICATOR_0:
		if (0U != io_read_pin(&LED_SIGNAL_INDICATOR_0)) {
			tmp = true;
		}
		break;

	case SIGNAL_INDICATOR_1:
		if (0U != io_read_pin(&LED_SIGNAL_INDICATOR_1)) {
			tmp = true;
		}
		break;

	case SIGNAL_INDICATOR_2:
		if (0U != io_read_pin(&LED_SIGNAL_INDICATOR_2)) {
			tmp = true;
		}
		break;

	case SIGNAL_INDICATOR_3:
		if (0U != io_read_pin(&LED_SIGNAL_INDICATOR_3)) {
			tmp = true;
		}
		break;

	case SIGNAL_INDICATOR_ALL:
		if ((0U != io_read_pin(&LED_SIGNAL_INDICATOR_0))
				&& (0U != io_read_pin(&LED_SIGNAL_INDICATOR_1))
				&& (0U != io_read_pin(&LED_SIGNAL_INDICATOR_2))
				&& (0U != io_read_pin(&LED_SIGNAL_INDICATOR_3))) {
			tmp = true;
		}
		break;

	default:
		;
		break;
	}

	return (tmp);
}

