#include "../mute.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_MUTE = { .port = &PORTB, .pin = PB4 };

void hw_mute_init(void)
{
	io_configure_as_output(&RELAY_MUTE);
	io_set_high(&RELAY_MUTE);
}

void hw_mute_on(void)
{
	io_set_high(&RELAY_MUTE);
}

void hw_mute_off(void)
{
	io_set_low(&RELAY_MUTE);
}

void hw_mute_toggle(void)
{
	io_set_opposite(&RELAY_MUTE);
}

bool hw_mute_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_MUTE)) ? false : true);
}

