#include "../input.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_INPUT_SIGNAL_0 = { .port = &PORTB, .pin = PB0 };
static io_pin_st const RELAY_INPUT_SIGNAL_1 = { .port = &PORTB, .pin = PB1 };
static io_pin_st const RELAY_INPUT_SIGNAL_2 = { .port = &PORTB, .pin = PB2 };
static io_pin_st const RELAY_INPUT_SIGNAL_3 = { .port = &PORTB, .pin = PB3 };

void hw_input_signal_init(void)
{
	io_configure_as_output(&RELAY_INPUT_SIGNAL_0);
	io_configure_as_output(&RELAY_INPUT_SIGNAL_1);
	io_configure_as_output(&RELAY_INPUT_SIGNAL_2);
	io_configure_as_output(&RELAY_INPUT_SIGNAL_3);

	io_set_low(&RELAY_INPUT_SIGNAL_0);
	io_set_low(&RELAY_INPUT_SIGNAL_1);
	io_set_low(&RELAY_INPUT_SIGNAL_2);
	io_set_low(&RELAY_INPUT_SIGNAL_3);
}

void hw_input_signal_on(input_signal_et input)
{
	switch (input) {
	case INPUT_SIGNAL_0:
		io_set_high(&RELAY_INPUT_SIGNAL_0);
		break;

	case INPUT_SIGNAL_1:
		io_set_high(&RELAY_INPUT_SIGNAL_1);
		break;

	case INPUT_SIGNAL_2:
		io_set_high(&RELAY_INPUT_SIGNAL_2);
		break;

	case INPUT_SIGNAL_3:
		io_set_high(&RELAY_INPUT_SIGNAL_3);
		break;

	default:
		;
		break;
	}
}

void hw_input_signal_off(input_signal_et input)
{
	switch (input) {
	case INPUT_SIGNAL_0:
		io_set_low(&RELAY_INPUT_SIGNAL_0);
		break;

	case INPUT_SIGNAL_1:
		io_set_low(&RELAY_INPUT_SIGNAL_1);
		break;

	case INPUT_SIGNAL_2:
		io_set_low(&RELAY_INPUT_SIGNAL_2);
		break;

	case INPUT_SIGNAL_3:
		io_set_low(&RELAY_INPUT_SIGNAL_3);
		break;

	default:
		;
		break;
	}
}

void hw_input_signal_toggle(input_signal_et input)
{
	switch (input) {
	case INPUT_SIGNAL_0:
		io_set_opposite(&RELAY_INPUT_SIGNAL_0);
		break;

	case INPUT_SIGNAL_1:
		io_set_opposite(&RELAY_INPUT_SIGNAL_1);
		break;

	case INPUT_SIGNAL_2:
		io_set_opposite(&RELAY_INPUT_SIGNAL_2);
		break;

	case INPUT_SIGNAL_3:
		io_set_opposite(&RELAY_INPUT_SIGNAL_3);
		break;

	default:
		;
		break;
	}
}

bool hw_input_signal_is_on(input_signal_et input)
{
	bool tmp = false;

	switch (input) {
	case INPUT_SIGNAL_0:
		if (0U != io_read_pin(&RELAY_INPUT_SIGNAL_0)) {
			tmp = true;
		}
		break;

	case INPUT_SIGNAL_1:
		if (0U != io_read_pin(&RELAY_INPUT_SIGNAL_1)) {
			tmp = true;
		}
		break;

	case INPUT_SIGNAL_2:
		if (0U != io_read_pin(&RELAY_INPUT_SIGNAL_2)) {
			tmp = true;
		}
		break;

	case INPUT_SIGNAL_3:
		if (0U != io_read_pin(&RELAY_INPUT_SIGNAL_3)) {
			tmp = true;
		}
		break;

	default:
		;
		break;
	}

	return (tmp);
}

