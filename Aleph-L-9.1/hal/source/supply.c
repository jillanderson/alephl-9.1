#include "../supply.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_SUPPLY = { .port = &PORTA, .pin = PA7 };

void hw_supply_init(void)
{
	io_configure_as_output(&RELAY_SUPPLY);
	io_set_low(&RELAY_SUPPLY);
}

void hw_supply_on(void)
{
	io_set_high(&RELAY_SUPPLY);
}

void hw_supply_off(void)
{
	io_set_low(&RELAY_SUPPLY);
}

bool hw_supply_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_SUPPLY)) ? false : true);
}

