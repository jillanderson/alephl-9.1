#include "../attenuator.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_ATTENUATOR_1DB = { .port = &PORTA, .pin = PA1 };
static io_pin_st const RELAY_ATTENUATOR_2DB = { .port = &PORTA, .pin = PA2 };
static io_pin_st const RELAY_ATTENUATOR_4DB = { .port = &PORTA, .pin = PA3 };
static io_pin_st const RELAY_ATTENUATOR_8DB = { .port = &PORTA, .pin = PA4 };
static io_pin_st const RELAY_ATTENUATOR_16DB = { .port = &PORTA, .pin = PA5 };
static io_pin_st const RELAY_ATTENUATOR_32DB = { .port = &PORTA, .pin = PA6 };

void hw_attenuator_init(void)
{
	io_configure_as_output(&RELAY_ATTENUATOR_32DB);
	io_configure_as_output(&RELAY_ATTENUATOR_16DB);
	io_configure_as_output(&RELAY_ATTENUATOR_8DB);
	io_configure_as_output(&RELAY_ATTENUATOR_4DB);
	io_configure_as_output(&RELAY_ATTENUATOR_2DB);
	io_configure_as_output(&RELAY_ATTENUATOR_1DB);

	io_set_high(&RELAY_ATTENUATOR_32DB);
	io_set_high(&RELAY_ATTENUATOR_16DB);
	io_set_high(&RELAY_ATTENUATOR_8DB);
	io_set_high(&RELAY_ATTENUATOR_4DB);
	io_set_high(&RELAY_ATTENUATOR_2DB);
	io_set_high(&RELAY_ATTENUATOR_1DB);
}

void hw_attenuator_on(attenuator_et attenuator)
{
	switch (attenuator) {
	case ATTENUATOR_1DB:
		io_set_high(&RELAY_ATTENUATOR_1DB);
		break;

	case ATTENUATOR_2DB:
		io_set_high(&RELAY_ATTENUATOR_2DB);
		break;

	case ATTENUATOR_4DB:
		io_set_high(&RELAY_ATTENUATOR_4DB);
		break;

	case ATTENUATOR_8DB:
		io_set_high(&RELAY_ATTENUATOR_8DB);
		break;

	case ATTENUATOR_16DB:
		io_set_high(&RELAY_ATTENUATOR_16DB);
		break;

	case ATTENUATOR_32DB:
		io_set_high(&RELAY_ATTENUATOR_32DB);
		break;

	default:
		;
		break;
	}
}

void hw_attenuator_off(attenuator_et attenuator)
{
	switch (attenuator) {
	case ATTENUATOR_1DB:
		io_set_low(&RELAY_ATTENUATOR_1DB);
		break;

	case ATTENUATOR_2DB:
		io_set_low(&RELAY_ATTENUATOR_2DB);
		break;

	case ATTENUATOR_4DB:
		io_set_low(&RELAY_ATTENUATOR_4DB);
		break;

	case ATTENUATOR_8DB:
		io_set_low(&RELAY_ATTENUATOR_8DB);
		break;

	case ATTENUATOR_16DB:
		io_set_low(&RELAY_ATTENUATOR_16DB);
		break;

	case ATTENUATOR_32DB:
		io_set_low(&RELAY_ATTENUATOR_32DB);
		break;

	default:
		;
		break;
	}
}

void hw_attenuator_toggle(attenuator_et attenuator)
{
	switch (attenuator) {
	case ATTENUATOR_1DB:
		io_set_opposite(&RELAY_ATTENUATOR_1DB);
		break;

	case ATTENUATOR_2DB:
		io_set_opposite(&RELAY_ATTENUATOR_2DB);
		break;

	case ATTENUATOR_4DB:
		io_set_opposite(&RELAY_ATTENUATOR_4DB);
		break;

	case ATTENUATOR_8DB:
		io_set_opposite(&RELAY_ATTENUATOR_8DB);
		break;

	case ATTENUATOR_16DB:
		io_set_opposite(&RELAY_ATTENUATOR_16DB);
		break;

	case ATTENUATOR_32DB:
		io_set_opposite(&RELAY_ATTENUATOR_32DB);
		break;

	default:
		;
		break;
	}
}

bool hw_attenuator_is_on(attenuator_et attenuator)
{
	bool tmp = false;

	switch (attenuator) {
	case ATTENUATOR_1DB:
		if (0U != io_read_pin(&RELAY_ATTENUATOR_1DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_2DB:
		if (0U != io_read_pin(&RELAY_ATTENUATOR_2DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_4DB:
		if (0U != io_read_pin(&RELAY_ATTENUATOR_4DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_8DB:
		if (0U != io_read_pin(&RELAY_ATTENUATOR_8DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_16DB:
		if (0U != io_read_pin(&RELAY_ATTENUATOR_16DB)) {
			tmp = true;
		}
		break;

	case ATTENUATOR_32DB:
		if (0U != io_read_pin(&RELAY_ATTENUATOR_32DB)) {
			tmp = true;
		}
		break;

	default:
		;
		break;
	}

	return (tmp);
}

