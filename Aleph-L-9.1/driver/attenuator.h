#pragma once

/** \file **************************************************************************************************************
 *
 * sterowanie tłumikiem sygnału
 *
 **********************************************************************************************************************/

#include <stdint.h>

/*
 * inicjalizuje tłumik sygnału
 * ustawia maksymalne tłumienie
 */
void attenuator_init(void);

/*
 * ustawia zadaną wartość tłumienia
 *
 * @value	wartość tłumienia w zakresie od 0dB do 63dB
 */
void attenuator_damping(uint8_t value);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void attenuator_on_tick_time(void);

