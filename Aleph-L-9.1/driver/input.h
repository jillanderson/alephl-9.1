#pragma once

/** \file **************************************************************************************************************
 *
 * sterowanie przełączaniem linii wejściowych
 *
 **********************************************************************************************************************/

#include "../hal/input.h"

#include <stdbool.h>

enum {
	MUTE_OFF = false,
	MUTE_ON = true
};

/*
 * inicjalizuje przełącznik wejść
 * odłącza  wszystkie wejścia, włącza wyciszenie
 */
void input_signal_init(void);

/*
 * ustawia linię wejściową i stan wyciszenia
 *
 * @input	zadana linia
 * @mute	status wyciszenia
 */
void input_signal_active(input_signal_et input, bool mute);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void input_signal_on_tick_time(void);

