#include "../../hal/signal.h"
#include "../signal.h"

#include <stdbool.h>
#include <stdint.h>

static struct {
	uint16_t on_time;
	uint16_t off_time;
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_RUNNING
	} counter_status;
	volatile signal_indicator_et active;
} signal_indicator = { 0 };

static void switch_state(void);

void signal_indicator_init(void)
{
	hw_signal_indicator_init();
	signal_indicator.active = SIGNAL_INDICATOR_ALL;
}

void signal_indicator_on(signal_indicator_et signal)
{
	signal_indicator.counter_status = COUNTER_STOPPED;
	signal_indicator.active = signal;
	hw_signal_indicator_off(SIGNAL_INDICATOR_ALL);
	hw_signal_indicator_on(signal_indicator.active);
}

void signal_indicator_off(void)
{
	signal_indicator.counter_status = COUNTER_STOPPED;
	hw_signal_indicator_off(SIGNAL_INDICATOR_ALL);
}

void signal_indicator_blink(uint16_t on_time_ms, uint16_t off_time_ms)
{
	signal_indicator.on_time = on_time_ms;
	signal_indicator.off_time = off_time_ms;
	switch_state();
	signal_indicator.counter_status = COUNTER_RUNNING;
}

void signal_indicator_on_tick_time(void)
{
	if (COUNTER_RUNNING == signal_indicator.counter_status) {

		if (0U < signal_indicator.time_counter) {
			signal_indicator.time_counter--;
		}

		if (0U == signal_indicator.time_counter) {
			switch_state();
		}
	}
}

void switch_state(void)
{
	hw_signal_indicator_toggle(signal_indicator.active);
	signal_indicator.time_counter = (true == hw_signal_indicator_is_on(signal_indicator.active)) ?
			signal_indicator.on_time : signal_indicator.off_time;
}

