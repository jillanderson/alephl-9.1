#include "../../hal/encoder.h"
#include "../encoder.h"

#include <common/encoder_event_detector.h>

static struct {
	encoder_event_detector_st input;
	encoder_event_detector_st volume;
} encoder_detector = { 0 };

void encoder_init(void)
{
	hw_encoder_init();

	encoder_event_detector_init(&(encoder_detector.input));
	encoder_event_detector_init(&(encoder_detector.volume));
}

void encoder_handler(encoder_et encoder, encoder_event_handler_ft *handler, void *arg)
{
	switch (encoder) {
	case ENCODER_INPUT:
		encoder_event_detector_handler(&(encoder_detector.input), handler, arg);
		break;

	case ENCODER_VOLUME:
		encoder_event_detector_handler(&(encoder_detector.volume), handler, arg);
		break;

	default:
		;
		break;
	}
}

void encoder_on_tick_time(void)
{
	encoder_event_detector_time_tick(&(encoder_detector.input), hw_encoder_read_state(ENCODER_INPUT));
	encoder_event_detector_time_tick(&(encoder_detector.volume), hw_encoder_read_state(ENCODER_VOLUME));
	encoder_event_detector_dispatch(&(encoder_detector.input));
	encoder_event_detector_dispatch(&(encoder_detector.volume));
}

