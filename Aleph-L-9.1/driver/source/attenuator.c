#include "../../hal/attenuator.h"
#include "../attenuator.h"

#include <stdint.h>

/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
static uint8_t const RELAY_BOUNCE_TIME_MS = 15U;

static struct {
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_RUNNING
	} counter_status;
	volatile enum {
		SETTING_DIFF,
		SETTING_VALUE
	} switching_stage;
	volatile uint8_t next;
	volatile uint8_t tmp;
	volatile uint8_t diff;
} attenuator = { 0 };

void attenuator_init(void)
{
	hw_attenuator_init();
}

void attenuator_damping(uint8_t value)
{
	attenuator.counter_status = COUNTER_STOPPED;

	uint8_t const damping_max = ((1U << ATTENUATOR_QTY) - 1U);

	attenuator.next = value;

	if (damping_max < value) {
		attenuator.next = damping_max;
	}
	/* _Bool jest rozwijany do wartości int: true = 0x01, false = 0x00 */
	attenuator.tmp = (uint8_t) (((hw_attenuator_is_on(ATTENUATOR_32DB)) << ATTENUATOR_32DB)
	                | ((hw_attenuator_is_on(ATTENUATOR_16DB)) << ATTENUATOR_16DB)
	                | ((hw_attenuator_is_on(ATTENUATOR_8DB)) << ATTENUATOR_8DB)
	                | ((hw_attenuator_is_on(ATTENUATOR_4DB)) << ATTENUATOR_4DB)
	                | ((hw_attenuator_is_on(ATTENUATOR_2DB)) << ATTENUATOR_2DB)
	                | ((hw_attenuator_is_on(ATTENUATOR_1DB)) << ATTENUATOR_1DB));

	attenuator.diff = attenuator.next | attenuator.tmp;

	attenuator.switching_stage = SETTING_DIFF;
	attenuator.time_counter = 0U;
	attenuator.counter_status = COUNTER_RUNNING;
}

void attenuator_on_tick_time(void)
{
	if (COUNTER_RUNNING == attenuator.counter_status) {
		if (0U < attenuator.time_counter) {
			attenuator.time_counter--;
		}

		if (0U == attenuator.time_counter) {
			attenuator.time_counter = RELAY_BOUNCE_TIME_MS;

			switch (attenuator.switching_stage) {
			case SETTING_DIFF:
				for (uint8_t i = ATTENUATOR_QTY; 0U < i; i--) {
					if ((attenuator.tmp & (1U << (i - 1U))) != (attenuator.diff & (1U << (i - 1U)))) {
						attenuator.tmp |= (uint8_t) (1U << (i - 1U));
						hw_attenuator_on((i - 1U));
						break;
					}
				}

				if ((attenuator.tmp) == (attenuator.diff)) {
					(attenuator.switching_stage) = SETTING_VALUE;
				}

				break;

			case SETTING_VALUE:
				for (uint8_t i = 0U; ATTENUATOR_QTY > i; i++) {
					if (((1U << i) == (attenuator.tmp & (1U << i))) && (0U == (attenuator.next & (1U << i)))) {
						attenuator.tmp &= (uint8_t) (~(1U << i));
						hw_attenuator_off(i);
						break;
					}
				}

				if ((attenuator.tmp) == (attenuator.next)) {
					attenuator.counter_status = COUNTER_STOPPED;
				}

				break;

			default:
				;
				break;
			}
		}
	}
}

