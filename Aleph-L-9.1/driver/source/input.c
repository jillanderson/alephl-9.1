#include "../../hal/input.h"
#include "../input.h"

#include <stdbool.h>
#include <stdint.h>

#include "../../hal/mute.h"

/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
static uint8_t const RELAY_BOUNCE_TIME_MS = 15U;

static struct {
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_RUNNING
	} counter_status;
	volatile enum {
		SETTING_INPUT,
		SETTING_MUTE
	} switching_stage;
	input_signal_et input_active;
	bool mute_status;
} input_signal_selector = { 0 };

void input_signal_init(void)
{
	hw_input_signal_init();
	hw_mute_init();

	input_signal_selector.input_active = INPUT_SIGNAL_NONE;
	input_signal_selector.mute_status = MUTE_ON;
}

void input_signal_active(input_signal_et input, bool mute)
{
	hw_mute_on();

	input_signal_selector.counter_status = COUNTER_STOPPED;
	input_signal_selector.input_active = input;
	input_signal_selector.mute_status = mute;
	input_signal_selector.time_counter = RELAY_BOUNCE_TIME_MS;
	input_signal_selector.switching_stage = SETTING_INPUT;
	input_signal_selector.counter_status = COUNTER_RUNNING;
}

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void input_signal_on_tick_time(void)
{
	if (COUNTER_RUNNING == input_signal_selector.counter_status) {
		if (0U < input_signal_selector.time_counter) {
			input_signal_selector.time_counter--;
		}

		if (0U == input_signal_selector.time_counter) {
			switch (input_signal_selector.switching_stage) {
			case SETTING_INPUT:
				hw_input_signal_off(INPUT_SIGNAL_0);
				hw_input_signal_off(INPUT_SIGNAL_1);
				hw_input_signal_off(INPUT_SIGNAL_2);
				hw_input_signal_off(INPUT_SIGNAL_3);

				if (INPUT_SIGNAL_ALL > input_signal_selector.input_active) {
					hw_input_signal_on(input_signal_selector.input_active);
				} else if (INPUT_SIGNAL_ALL == input_signal_selector.input_active) {
					hw_input_signal_on(INPUT_SIGNAL_0);
					hw_input_signal_on(INPUT_SIGNAL_1);
					hw_input_signal_on(INPUT_SIGNAL_2);
					hw_input_signal_on(INPUT_SIGNAL_3);
				} else {
					; /* INPUT_SIGNAL_NONE */
				}

				input_signal_selector.time_counter = RELAY_BOUNCE_TIME_MS;
				input_signal_selector.switching_stage = SETTING_MUTE;
				break;

			case SETTING_MUTE:
				if (MUTE_OFF == input_signal_selector.mute_status) {
					hw_mute_off();
				}

				input_signal_selector.counter_status = COUNTER_STOPPED;
				break;

			default:
				;
				break;
			}
		}
	}
}

